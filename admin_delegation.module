<?php
// $Id: admin_delegation.module,v 1.3 2009/06/08 22:12:13 deekayen Exp $

/**
 * @file
 * admin_delegation - a Drupal module that allows site administrators to
 * further delegate the task of managing user accounts
 *
 * @author David Kent Norman
 * @link http://deekayen.net/
 * @link http://www.cgraphics.com/
 */

/**
 * Alternative to user_access(), specific to permissions of admin_delegtation.
 *
 * @param string $role
 * @return int How many rows from the admin_delegation permission table match the type of role being questioned by the param.
 */
function admin_delegation_access($role) {
  global $user;

  $result = db_query("SELECT COUNT(udp.rid) FROM {admin_delegation_permission} udp WHERE udp.rid IN (%s) AND udp.field_id = '%s'", implode(',', array_keys($user->roles)), $role);
  return db_result($result);
}

/**
 * Implementation of hook_menu().
 */
function admin_delegation_menu() {
  $items = array();
  $items['admin/user/admin_delegation'] = array(
    'title' => 'Admin delegation',
    'description' => 'Allows site administrators to further delegate the task of managing users.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('admin_delegation_rules'),
    'access callback' => 'user_access',
    'access arguments' => array('administer permissions')
  );
  $items['admin/user/admin_delegation/profile'] = array(
    'title' => 'Account edits',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin_delegation/%/status'] = array(
    'title' => 'change status',
    'page callback' => 'admin_delegation_status_change',
    'page arguments' => array(1),
    'type' => MENU_CALLBACK,
    'access callback' => 'admin_delegation_access',
    'access arguments' => array('status')
  );
  $items['admin_delegation/%/%/add'] = array(
    'title' => 'change role',
    'page callback' => 'admin_delegation_role_add',
    'page arguments' => array(1, 2),
    'type' => MENU_CALLBACK,
    'access callback' => 'admin_delegation_access',
    'access arguments' => array(2)
  );
  $items['admin_delegation/%/%/remove'] = array(
    'title' => 'change role',
    'page callback' => 'admin_delegation_role_remove',
    'page arguments' => array(1, 2),
    'type' => MENU_CALLBACK,
    'access' => 'admin_delegation_access',
    'access arguments' => array(2)
  );
  return $items;
}

/**
 * Add a role for a user to the database
 *
 * @param int $uid user id
 * @param int $rid role id
 */
function admin_delegation_role_add($uid, $rid) {
  db_query('REPLACE INTO {users_roles} (`uid`, `rid`) VALUES (%d, %d)', $uid, $rid);
  drupal_set_message(t('Account role added'));
  drupal_goto("user/$uid");
}

/**
 * Remove a role from a user
 *
 * @param int $uid user id
 * @param int $rid role id
 */
function admin_delegation_role_remove($uid, $rid) {
  db_query('DELETE FROM {users_roles} WHERE uid = %d AND rid = %d', $uid, $rid);
  drupal_set_message(t('Account role removed'));
  drupal_goto("user/$uid");
}

/**
 * Change the status setting in the DB from 0 to 1 or vise versa
 *
 * @param int $uid user id
 */
function admin_delegation_status_change($uid) {
  db_query('UPDATE {users} SET status = status ^ 1 WHERE uid = %d', $uid);
  drupal_set_message(t('Account status updated'));
  drupal_goto("user/$uid");
}

/**
 * Adminstration settings form to provide the option of restricting
 * users from editing the status or roles on their own account.
 *
 * @return array
 */
function admin_delegation_settings() {
  $form = array();

  $form['admin_delegation_block_self'] = array(
    '#type' => checkbox,
    '#title' => t('Block own account edits'),
    '#description' => t('Users with delegated powers would not be able to change the status or role on their own account with this option activated. This will only apply to the account view tab, not the edit tab.'),
    '#default_value' => variable_get('admin_delegation_block_self', 1)
  );

  return $form;
}

/**
 * Implementation of hook_user().
 *
 * Inserts links for editing users' status or role into the view profile page
 * where the viewing user has the proper permission to perform such operations.
 */
function admin_delegation_user($type, &$edit, &$param_user, $category = NULL) {
  global $user;

  if (isset($category)) {
    return;
  }
/*  if ($type == 'load') {
    if (!isset($param_user->original_status)) {
      $param_user->original_status = $param_user->status;
    }
  } */
  if ($type == 'view') {
    $items = array();

    $block_self = variable_get('admin_delegation_block_self', 1);
    if (!empty($block_self) && $param_user->uid == $user->uid) {
      return;
    }
    unset($block_self);

//    $status_value = $param_user->original_status ? t('Active') : t('Blocked');
//    $status_value .= ' ['. l($param_user->original_status ? t('change to blocked') : t('change to active') , "admin_delegation/$param_user->uid/status") .']';
    $status_value = $param_user->status ? t('Active') : t('Blocked');
    $status_value .= ' ['. l($param_user->status ? t('change to blocked') : t('change to active') , "admin_delegation/$param_user->uid/status") .']';

    $role_keys = implode(',', array_keys($user->roles));

    $param_user->content['admin_delegation'] = array(
      '#type' => 'user_profile_category',
      '#title' => t('Account Administration'),
      '#attributes' => array('class' => 'admin_delegation')
    );

    $result = db_query("SELECT COUNT(p.rid) FROM {admin_delegation_permission} p WHERE p.rid IN (%s) AND p.field_id = 'status' AND p.field_type = 'other'", $role_keys);
    if (db_result($result) > 0) {
      $param_user->content['admin_delegation']['user_delegate_status_change'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Status'),
        '#value' => $status_value,
        '#weight' => 0,
        '#attributes' => array('class' => 'admin_delegation')
      );
    }

    $result = db_query("SELECT COUNT(p.field_id) FROM {admin_delegation_permission} p WHERE p.rid IN (%s) AND p.field_type = 'role'", $role_keys);
    if (db_result($result) > 0) {
      $param_user->content['admin_delegation']['user_delegate_role_change'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Roles'),
        '#value' => '',
        '#weight' => 1,
        '#attributes' => array('class' => 'admin_delegation')
      );
    }

    $roles = user_roles(TRUE);
    $result = db_query("SELECT p.field_id FROM {admin_delegation_permission} p WHERE p.rid IN (%s) AND p.field_type = 'role'", $role_keys);
    while ($perm = db_fetch_object($result)) {
      if ($perm->field_id == 1) {
        continue;
      }
      $param_user->content['admin_delegation']["user_delegate_role_change {$roles[$perm->field_id]}"] = array(
        '#type' => 'user_profile_item',
        '#title' => '',
        '#value' => $roles[$perm->field_id] .' ['. (isset($param_user->roles[$perm->field_id]) ? l(t('remove from !role role', array('!role' => $roles[$perm->field_id])), "admin_delegation/$param_user->uid/$perm->field_id/remove") : l(t('add to !role role', array('!role' => $roles[$perm->field_id])), "admin_delegation/$param_user->uid/$perm->field_id/add")) .']',
        '#weight' => 2,
        '#attributes' => array('class' => 'admin_delegation')
      );
    }
  }
}

/**
 * Returns an array of supported administative changes by default
 *
 * Param can set which change type to call up more details about, which
 * is eventually used to populate the field_id field in the database
 *
 * @param string $type
 * @return array
 */
function _admin_delegation_field_types($type = NULL) {
  switch ($type) {
    //future implementation
/*    case 'name':
      return array('name');
      break;
    case 'mail':
      return array('mail');
      break;
    case 'pass':
      return array('pass');
      break;
    case 'contact':
      return array('contact');
      break;
    case 'comment':
      return array('signature');
      break;
    case 'locale':
      return array('timezone');
      break; // */
    case 'status':
      return array('status');
      break;
    case 'role':
      return _admin_delegation_roles();
    default:
    return array(
//      'name',
//      'mail',
//      'pass',
//      'contact',
//      'comment',
//      'locale',
      'status',
      'role'
    );
  }
}

function _admin_delegation_roles() {
  $perm = array();
  $result = db_query('SELECT r.rid, r.name FROM {role} r WHERE r.rid NOT IN ('. db_placeholders(array(DRUPAL_AUTHENTICATED_RID, DRUPAL_ANONYMOUS_RID)) .') ORDER BY r.name', DRUPAL_AUTHENTICATED_RID, DRUPAL_ANONYMOUS_RID);
  while ($role = db_fetch_object($result)) {
    $perm[$role->rid] = $role->name;
  }
  return $perm;
}

/**
 * Generates an array of form content to display
 * at admin/user/admin_delegation. Form values are themed
 * into a table as part of a separate function.
 *
 * @return array
 */
function admin_delegation_rules() {
  $form = array();

  $result = db_query('SELECT r.rid, p.field_id FROM {role} r LEFT JOIN {admin_delegation_permission} p ON r.rid = p.rid ORDER BY r.name');

  // Compile role array:
  // Add a comma at the end so when searching for a permission, we can
  // always search for "$perm," to make sure we do not confuse
  // permissions that are substrings of each other.
  while ($role = db_fetch_object($result)) {
    $role_permissions[$role->rid][] = $role->field_id;
  }

  // DRUPAL_ANONYMOUS_RID and DRUPAL_AUTHENTICATED_RID filtering was intentionally left
  // out of this query so those users could assign other roles, but could not assign
  // anonymous or authenticated roles since those are more like a state of being logged in or not
  $result = db_query('SELECT r.rid, r.name FROM {role} r ORDER BY r.name');
  $role_names = array();
  while ($role = db_fetch_object($result)) {
    $role_names[$role->rid] = $role->name;
  }

  $options = array();
  $status  = array();

  foreach (_admin_delegation_field_types() as $field_type) {
    $form['permission'][] = array('#type' => 'markup', '#value' => t($field_type));
    foreach (_admin_delegation_field_types($field_type) as $inner_role) {
      $options[$inner_role] = '';
      $form['permission'][$inner_role] = array(
        '#value' => t($inner_role)
      );
      foreach ($role_names as $rid => $name) {
        if (in_array($inner_role, $role_permissions[$rid])) {
          $status[$rid][] = $inner_role;
        }
      }
    }
  }

  foreach ($role_names as $outer_role) { // $outer_role not used
    foreach ($role_names as $option_rid => $inner_role) {
      if ($option_rid == DRUPAL_ANONYMOUS_RID || $option_rid == DRUPAL_AUTHENTICATED_RID) {
        // skips creating a row for the anonymous and authenticated user roles
        // since their status is unchangeable
        continue;
      }

      $options[$inner_role] = '';
      $form['permission'][$inner_role] = array('#type' => 'markup', '#value' => t($inner_role));
      foreach ($role_names as $rid => $name) {
        // Builds arrays for checked boxes for each role
        if (in_array($option_rid, $role_permissions[$rid])) {
          $status[$rid][] = $inner_role;
        }
      }
    }
  }

  unset($outer_role, $rid, $name, $inner_role);

  // Have to build checkboxes here after checkbox arrays are built
  foreach ($role_names as $rid => $name) {
    $form['checkboxes'][$rid] = array('#type' => 'checkboxes', '#options' => $options, '#default_value' => isset($status[$rid]) ? $status[$rid] : array());
    $form['role_names'][$rid] = array('#value' => $name, '#tree' => TRUE);
  }
  unset($rid, $name);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save permissions'));

  return $form;
}

/**
 * Implementation of hook_theme()
 *
 * Registers the theme function that puts the admin settings form
 * into a table/grid.
 *
 * @return array
 */
function admin_delegation_theme() {
  return array(
    'admin_delegation_rules' => array(
      'arguments' => array('form' => NULL)
    )
  );
}

/**
 * Puts the administration settings form into a table/grid.
 *
 * @param array $form
 * @return string
 */
function theme_admin_delegation_rules($form) {
  foreach (element_children($form['permission']) as $key) {
    // Don't take form control structures
    if (isset($form['permission'][$key]) && is_array($form['permission'][$key])) {
      $row = array();
      if (is_numeric($key)) {
        $row[] = array('data' => drupal_render($form['permission'][$key]), 'class' => 'module', 'colspan' => count($form['role_names']) + 1);
      }
      else {
        $row[] = array('data' => drupal_render($form['permission'][$key]), 'class' => 'permission');
        foreach (element_children($form['checkboxes']) as $rid) {
          if (is_array($form['checkboxes'][$rid])) {
            $row[] = array('data' => drupal_render($form['checkboxes'][$rid][$key]), 'class' => 'checkbox', 'align' => 'center', 'title' => t($key));
          }
        }
      }
      $rows[] = $row;
    }
  }
  $header[] = (t('Permission'));
  foreach (element_children($form['role_names']) as $rid) {
    if (is_array($form['role_names'][$rid])) {
      $header[] = array('data' => drupal_render($form['role_names'][$rid]), 'class' => 'checkbox');
    }
  }
  $output = theme('table', $header, $rows, array('id' => 'permissions'));
  $output .= drupal_render($form);
  return $output;
}

/**
 * Saves the administration permissions to the database.
 * Deletes all existing permissions before refilling them,
 * so transaction support in the future would be good.
 *
 * @todo add transaction support
 */
function admin_delegation_rules_submit($form, &$form_state) {
  // Save permissions:
  $roles = _admin_delegation_roles();
  while (list($rid, ) = each($roles)) {
    if (isset($form_state['values'][$rid])) {
      db_query('DELETE FROM {admin_delegation_permission} WHERE rid = %d', $rid);
      foreach ($form_state['values'][$rid] as $key => $value) {
        if (!$value) {
          unset($form_state['values'][$rid][$key]);
        }
      }
      if (count($form_state['values'][$rid])) {
        foreach ($form_state['values'][$rid] as $key => $value) { // $key should == $value
          $not_role = array('name', 'mail', 'pass', 'contact', 'signature', 'timezone', 'status');

          if (in_array($key, $not_role)) {
            db_query("INSERT INTO {admin_delegation_permission} (rid, field_id, field_type) VALUES (%d, '%s', '%s')", $rid, $key, 'other');
          }
          else {
            $rid_search = array_search($key, $roles);
            if ($rid_search !== false) {
              db_query("INSERT INTO {admin_delegation_permission} (rid, field_id, field_type) VALUES (%d, '%s', '%s')", $rid, $rid_search, 'role');
            }
          }
        }
      }
    }
  }

  drupal_set_message(t('The changes have been saved.'));
}
